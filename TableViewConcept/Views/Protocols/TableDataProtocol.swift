//
//  TableDataProtocol.swift
//  TableViewConcept
//
//  Created by Rahul Kumawat on 08/11/21.
//

import Foundation

protocol TableDataProtocol {
    func didReceiveData(data: [CountryModel])
    func didFilterData(isFiltered: Bool)
}
