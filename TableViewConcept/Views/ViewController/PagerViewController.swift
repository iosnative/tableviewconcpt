//
//  PagerViewController.swift
//  TableViewConcept
//
//  Created by Rahul Kumawat on 12/11/21.
//

import UIKit

class PagerViewController: UIViewController {

    static var getInstance:PagerViewController{
        return (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "PagerViewController")) as! PagerViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        // self.tableView.deselectSelectedRow(animated: true)
    }

}
