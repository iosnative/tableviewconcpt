//
//  DetailsViewController.swift
//  TableViewConcept
//
//  Created by Rahul Kumawat on 03/11/21.
//

import UIKit
import SVGKit

class DetailsViewController: UIViewController {
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var code: UILabel!
    
    var data: CountryModel?
    
    static var getInstance:DetailsViewController{
        return (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "DetailsViewController")) as! DetailsViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        name.text = data?.name
        code.text = "\(data?.code?.uppercased() ?? "") \(data?.emoji ?? "")"
        let svgURL = URL(string: (data?.image)!)
        let data = try? Data(contentsOf: svgURL!)
        let anSVGImage: SVGKImage = SVGKImage(data: data!)
        image.image = anSVGImage.uiImage
//        code.text = "\(data?.code) \(data?.emoji)"
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
}
