//
//  ContinentViewController.swift
//  TableViewConcept
//
//  Created by Rahul Kumawat on 10/11/21.
//

import UIKit

class ContinentViewController: UIViewController {

    static var getInstance:ContinentViewController{
        return (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "ContinentViewController")) as! ContinentViewController
    }
    
    @IBOutlet weak var cTableView: UITableView!
        
    var viewModelCountry = CountryViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        cTableView.register(UINib(nibName: "ContinentTableViewCell", bundle: nil), forCellReuseIdentifier: "ContinentTableViewCell")
        viewModelCountry.continentvc = self
        loadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
//        self.tableView.deselectSelectedRow(animated: true)
    }
    
    func loadData()  {
        DispatchQueue.main.async {
            self.viewModelCountry.getContinentName()
            self.viewModelCountry.getContinentCountryName()
        }
    }
    
    @IBAction func cDidTapCollectionButton(_ sender: Any) {
        let vc = CollectionViewController.getInstance
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func cDidTapListButton(_ sender: Any) {
        let vc = MainTableViewController.getInstance
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
}

extension ContinentViewController: UITableViewDataSource, UITableViewDelegate{

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModelCountry.continentNameArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = cTableView.dequeueReusableCell(withIdentifier: "ContinentTableViewCell", for: indexPath) as? ContinentTableViewCell
        cell?.modelContinentName = viewModelCountry.continentNameArray[indexPath.row]
        cell?.tableCellIndex = indexPath.row
        cell?.navigationController = navigationController
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 350
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }

}
