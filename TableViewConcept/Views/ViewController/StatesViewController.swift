//
//  StatesViewController.swift
//  TableViewConcept
//
//  Created by Rahul Kumawat on 15/11/21.
//

import UIKit

class StatesViewController: UIViewController {
    
    var continentName = ""
    var totalCountry = ""
    var countryName = ""
    var totalStates = ""
    var states: [String] = []
    
    @IBOutlet weak var continentNameLabel: UILabel!
    @IBOutlet weak var totalCountryLabel: UILabel!
    @IBOutlet weak var countryNameLabel: UILabel!
    @IBOutlet weak var totalStatesLabel: UILabel!
    
    @IBOutlet weak var stateTableView: UITableView!
    
    static var getInstance:StatesViewController{
        return (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "StatesViewController")) as! StatesViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        stateTableView.register(UINib(nibName: "StateTableViewCell", bundle: nil), forCellReuseIdentifier: "StateTableViewCell")
        self.navigationController?.isNavigationBarHidden = false
        continentNameLabel.text = continentName
        totalCountryLabel.text = totalCountry
        countryNameLabel.text = countryName
        totalStatesLabel.text = totalStates
        if(states.count == 0){
            msgLabel()
        }
    }
    
    func msgLabel() {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 250, height: 21))
        label.center = CGPoint(x: view.frame.width/2 , y: view.frame.height/2)
        label.textAlignment = .center
        label.text = "Data not available"
        label.textColor = .gray
        self.view.addSubview(label)
    }
    
}

extension StatesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return states.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StateTableViewCell", for: indexPath ) as? StateTableViewCell
        cell?.stateNameLabel?.text = "\(indexPath.row+1). \(states[indexPath.row])"
        return cell!
    }
    
}
