//
//  MainTableViewController.swift
//  TableViewConcept
//
//  Created by Rahul Kumawat on 02/11/21.
//

import UIKit

class MainTableViewController: UIViewController, TableDataProtocol {
    
    var receivedDataArray : [CountryModel] = []
    var isFiltered : Bool = false
    
    @IBOutlet weak var tableView: UITableView!
    var viewModelCountry = CountryViewModel()
    
    // child VC
    let headerViewController = HeaderViewController()
    let searchViewController = SearchViewController()

    static var getInstance:MainTableViewController{
        return (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "MainTableViewController")) as! MainTableViewController
    }

    // View which contains the loading text and the spinner
    let loadingView = UIView()
    // Spinner shown during load the TableView
    let spinner = UIActivityIndicatorView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        addInitialChildView()
        tableView.register(UINib(nibName: "CountryCell", bundle: nil), forCellReuseIdentifier: "CountryCell")
        viewModelCountry.tablevc = self
        searchViewController.dataDelegate = self
        setLoadingScreen()
        loadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
//        self.tableView.deselectSelectedRow(animated: true)
    }
    
    func addInitialChildView(){
        addHeaderChildVc()
        addSearchChildVc()
    }
    
    func addHeaderChildVc() {
        addChild(headerViewController)
        headerViewController.didMove(toParent: self)
        view.addSubview(headerViewController.view)
        setHeaderChildVcConstrains()
    }
    
    func setHeaderChildVcConstrains() {
        headerViewController.view.translatesAutoresizingMaskIntoConstraints = false
        headerViewController.view.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
        headerViewController.view.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
        headerViewController.view.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
        headerViewController.view.heightAnchor.constraint(equalToConstant: 40).isActive = true
    }
    
    func addSearchChildVc() {
        addChild(searchViewController)
        view.addSubview(searchViewController.view)
        setSearchChildVcConstrains()
    }
    
    func setSearchChildVcConstrains() {
        searchViewController.view.translatesAutoresizingMaskIntoConstraints = false
        searchViewController.view.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
        searchViewController.view.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
        searchViewController.view.topAnchor.constraint(equalTo: headerViewController.view.bottomAnchor, constant: 0).isActive = true
        searchViewController.view.bottomAnchor.constraint(equalTo: tableView.topAnchor, constant: -6).isActive = true
        searchViewController.view.heightAnchor.constraint(equalToConstant: 44).isActive = true
    }
    
    @IBAction func didTabCollection(_ sender: Any) {
        let vc = CollectionViewController.getInstance
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func didTapContinentButton(_ sender: Any) {
        let vc = ContinentViewController.getInstance
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func loadData()  {
        DispatchQueue.main.async {
            self.viewModelCountry.getAllCountryData()
        }
    }
    
    // Set the activity indicator into the main view
    func setLoadingScreen() {
        // Sets the view which contains the loading text and the spinner
        let width: CGFloat = 30
        let height: CGFloat = 30
        let x = (tableView.frame.width / 2) - (width / 2)
        let y = (tableView.frame.height / 2) - (height / 2)
        loadingView.frame = CGRect(x: x, y: y, width: width, height: height)
        
        // Sets spinner
        spinner.style = .large
        spinner.color = .systemRed
        spinner.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        spinner.startAnimating()
        
        // Adds text and spinner to the view
        loadingView.addSubview(spinner)
        tableView.addSubview(loadingView)
    }
    
    // Remove the activity indicator from the main view
    func removeLoadingScreen() {
        // Hides and stops the text and the spinner
        spinner.stopAnimating()
        spinner.isHidden = true
    }
    
}

extension MainTableViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        searchViewController.countryLocalArray = viewModelCountry.countryArray
        if(isFiltered){
            //print("COUNT Filtered list : ",searchViewController.filteredCountryArray.count)
            return self.receivedDataArray.count
        }
        else{
            //print("COUNT Normal list : ",viewModelCountry.countryArray.count)
            return viewModelCountry.countryArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CountryCell", for: indexPath ) as? CountryCell
        if(isFiltered){
            cell?.modelCountry = self.receivedDataArray[indexPath.row]
        }
        else{
            cell?.modelCountry = viewModelCountry.countryArray[indexPath.row]
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        print(indexPath.row)
        let cell = tableView.cellForRow(at: indexPath)
        if cell?.isSelected == true { // check if cell has been previously selected
            tableView.deselectRow(at: indexPath, animated: true)
        }
        let vc = DetailsViewController.getInstance
        vc.modalPresentationStyle = .fullScreen
        if(isFiltered){
            vc.data = self.receivedDataArray[indexPath.row]
//            print(self.receivedDataArray[indexPath.row])
        }
        else{
            vc.data = viewModelCountry.countryArray[indexPath.row]
//            print(viewModelCountry.countryArray[indexPath.row])
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }

    func didReceiveData(data: [CountryModel]) {
        self.receivedDataArray = data
        //print("Data Receive : ",data)
    }
    
    func didFilterData(isFiltered: Bool) {
        self.isFiltered = isFiltered
        self.tableView.reloadData()
        //print("Data Filtered : ",isFiltered)
    }
    
}
