//
//  SplashViewController.swift
//  TableViewConcept
//
//  Created by Rahul Kumawat on 12/11/21.
//

import UIKit
import SwiftyGif

class SplashViewController: UIViewController {

    static var getInstance:SplashViewController{
        return (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "SplashViewController")) as! SplashViewController
    }
    
    @IBOutlet weak var splash_logo: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let gif = try! UIImage(gifName: "world_flag_gif.gif")
        self.splash_logo.setGifImage(gif, loopCount: -1)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5){
            self.redirectToHomePage()
        }
    }

    func redirectToHomePage() {
        let vc = PagerViewController.getInstance
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
