//
//  CollectionViewController.swift
//  TableViewConcept
//
//  Created by Rahul Kumawat on 09/11/21.
//

import UIKit

class CollectionViewController: UIViewController, TableDataProtocol {

    @IBOutlet weak var collectionView: UICollectionView!
    var viewModelCountry = CountryViewModel()
    
    var receivedDataArray : [CountryModel] = []
    var statesData : [StatesModel] = []
    
    var isFiltered : Bool = false
    
    // child VC
    let headerViewController = HeaderViewController()
    let searchViewController = SearchViewController()
    
    static var getInstance:CollectionViewController{
        return (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "CollectionViewController")) as! CollectionViewController
    }
    
    // View which contains the loading text and the spinner
    let loadingView = UIView()
    // Spinner shown during load the TableView
    let spinner = UIActivityIndicatorView()
    
    // ScrollView type
    let scrollingSettingView = UIView()
    let scrollingSettingButton = UIButton()
    private let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        addInitialChildView()
        collectionView.register(UINib(nibName: "CountryCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CountryCollectionViewCell")
        layout.scrollDirection = .vertical
        collectionView.collectionViewLayout = layout
        viewModelCountry.collectionvc = self
        searchViewController.dataDelegate = self
        setLoadingScreen()
        loadData()
        scrollingSettingButton.addTarget(self, action: #selector(changeDirection), for: .touchUpInside)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func addInitialChildView(){
        addHeaderChildVc()
        addSearchChildVc()
        addScrollButtonChildVc()
    }
    
    func addScrollButtonChildVc(){
        // add scroll setting view
        scrollingSettingView.backgroundColor = .systemBlue
        view.addSubview(scrollingSettingView)
        setScrollButtonViewChildVcConstrains()
        // add scroll setting button view
        scrollingSettingButton.clipsToBounds = false
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .vertical
        }
        if(layout.scrollDirection == .horizontal){
            scrollingSettingButton.setTitle("H", for: .normal)
        }
        else{
            scrollingSettingButton.setTitle("V", for: .normal)
        }
        scrollingSettingButton.titleLabel?.font = .systemFont(ofSize: 17, weight: .heavy)
        scrollingSettingView.addSubview(scrollingSettingButton)
        setScrollButtonChildVcConstrains()
    }
    
    func setScrollButtonViewChildVcConstrains() {
        scrollingSettingView.translatesAutoresizingMaskIntoConstraints = false
        scrollingSettingView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10).isActive = true
        scrollingSettingView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 10).isActive = true
        scrollingSettingView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        scrollingSettingView.widthAnchor.constraint(equalToConstant: 30).isActive = true
        scrollingSettingView.layer.cornerRadius = 15
    }
    
    func setScrollButtonChildVcConstrains() {
        scrollingSettingButton.translatesAutoresizingMaskIntoConstraints = false
        scrollingSettingButton.leadingAnchor.constraint(equalTo: scrollingSettingView.leadingAnchor, constant: 0).isActive = true
        scrollingSettingButton.trailingAnchor.constraint(equalTo: scrollingSettingView.trailingAnchor, constant: 0).isActive = true
        scrollingSettingButton.topAnchor.constraint(equalTo: scrollingSettingView.topAnchor, constant: 0).isActive = true
        scrollingSettingButton.bottomAnchor.constraint(equalTo: scrollingSettingView.bottomAnchor, constant: 0).isActive = true
        scrollingSettingButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        scrollingSettingButton.widthAnchor.constraint(equalToConstant: 30).isActive = true
        scrollingSettingButton.layer.cornerRadius = 15
    }
    
    @objc func changeDirection(){
        if(layout.scrollDirection == .horizontal){
            layout.scrollDirection = .vertical
            scrollingSettingButton.setTitle("H", for: .normal)
        }
        else{
            layout.scrollDirection = .horizontal
            scrollingSettingButton.setTitle("V", for: .normal)
        }
    }
    
    func addHeaderChildVc() {
        addChild(headerViewController)
        headerViewController.didMove(toParent: self)
        view.addSubview(headerViewController.view)
        setHeaderChildVcConstrains()
    }
    
    func setHeaderChildVcConstrains() {
        headerViewController.view.translatesAutoresizingMaskIntoConstraints = false
        headerViewController.view.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
        headerViewController.view.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
        headerViewController.view.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
        headerViewController.view.heightAnchor.constraint(equalToConstant: 40).isActive = true
    }
    
    func addSearchChildVc() {
        addChild(searchViewController)
        view.addSubview(searchViewController.view)
        setSearchChildVcConstrains()
    }
    
    func setSearchChildVcConstrains() {
        searchViewController.view.translatesAutoresizingMaskIntoConstraints = false
        searchViewController.view.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
        searchViewController.view.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
        searchViewController.view.topAnchor.constraint(equalTo: headerViewController.view.bottomAnchor, constant: 0).isActive = true
//        searchViewController.view.bottomAnchor.constraint(equalTo: tableView.topAnchor, constant: -10).isActive = true
        searchViewController.view.heightAnchor.constraint(equalToConstant: 44).isActive = true
    }

    @IBAction func didTapList(_ sender: Any) {
        let vc = MainTableViewController.getInstance
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func didTapContinentButton(_ sender: Any) {
        let vc = ContinentViewController.getInstance
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func loadData()  {
        DispatchQueue.main.async {
            self.viewModelCountry.getAllCountryData()
        }
    }
    
    // Set the activity indicator into the main view
    func setLoadingScreen() {
        // Sets the view which contains the loading text and the spinner
        let width: CGFloat = 30
        let height: CGFloat = 30
        let x = (collectionView.frame.width / 2) - (width / 2)
        let y = (collectionView.frame.height / 2) - (height / 2)
        loadingView.frame = CGRect(x: x, y: y, width: width, height: height)
        
        // Sets spinner
        spinner.style = .large
        spinner.color = .systemRed
        spinner.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        spinner.startAnimating()
        
        // Adds text and spinner to the view
        loadingView.addSubview(spinner)
        collectionView.addSubview(loadingView)
    }
    
    // Remove the activity indicator from the main view
    func removeLoadingScreen() {
        // Hides and stops the text and the spinner
        spinner.stopAnimating()
        spinner.isHidden = true
    }
    
}

extension CollectionViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let columnCount = 3
        let width  = (Int(view.frame.width) - 20) / columnCount
            return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //return viewModelCountry.countryArray.count
        searchViewController.countryLocalArray = viewModelCountry.countryArray
        
        if(isFiltered){
            //print("COUNT Filtered list : ",searchViewController.filteredCountryArray.count)
            return self.receivedDataArray.count
        }
        else{
            //print("COUNT Normal list : ",viewModelCountry.countryArray.count)
            return viewModelCountry.countryArray.count
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CountryCollectionViewCell", for: indexPath) as? CountryCollectionViewCell
        
        if(isFiltered){
            cell?.modelCountry = self.receivedDataArray[indexPath.row]
        }
        else{
            cell?.modelCountry = viewModelCountry.countryArray[indexPath.row]
        }
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        if cell?.isSelected == true {
            collectionView.deselectItem(at: indexPath, animated: true)
        }
        let vc = DetailsViewController.getInstance
        vc.modalPresentationStyle = .fullScreen
        if(isFiltered){
            vc.data = self.receivedDataArray[indexPath.row]
        }
        else{
            vc.data = viewModelCountry.countryArray[indexPath.row]
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func didReceiveData(data: [CountryModel]) {
        self.receivedDataArray = data
        //print("Data Receive : ",data)
    }
    
    func didFilterData(isFiltered: Bool) {
        self.isFiltered = isFiltered
        self.collectionView.reloadData()
    }
    
}
