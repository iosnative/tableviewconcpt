//
//  HeaderViewController.swift
//  TableViewConcept
//
//  Created by Rahul Kumawat on 03/11/21.
//

import UIKit

class HeaderViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        addHeaderImage()
    }
    
    func addHeaderImage(){
        let image = UIImage(named: "country_logo")
        let imageView = UIImageView(image: image!)
        self.view.addSubview(imageView)
        imageView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
        imageView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 75).isActive = true
        imageView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -75).isActive = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        self.view.layoutIfNeeded()
    }

}
