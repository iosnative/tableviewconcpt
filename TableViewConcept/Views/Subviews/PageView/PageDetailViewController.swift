//
//  PageDetailViewController.swift
//  TableViewConcept
//
//  Created by Rahul Kumawat on 12/11/21.
//

import UIKit

class PageDetailViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var welcomeButton: UIButton!
    
    //    @IBOutlet weak var titleLabel: UILabel!
    var index:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(index == 0){
            imageView.image = try! UIImage(imageName: "p1")
        }
        if(index == 1){
            imageView.image = try! UIImage(imageName: "p2")
        }
        if(index == 2){
            imageView.image = try! UIImage(imageName: "p3")
        }
        if(index == 3){
            imageView.image = try! UIImage(imageName: "p4")
        }
        if(index == 4){
            imageView.image = try! UIImage(imageName: "p5")
        }
        if(index == 5){
            welcomeLabel.isHidden = false
            welcomeButton.isHidden = false
        }
        else{
            welcomeLabel.isHidden = true
            welcomeButton.isHidden = true
        }
    }

    static func getInstance(index: Int) -> PageDetailViewController {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PageDetailViewController") as! PageDetailViewController
        vc.index = index
        return vc
    }
    
    @IBAction func didWelcomeButtonPressed(_ sender: Any) {
        let vc = MainTableViewController.getInstance
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}
