//
//  CustomPageViewController.swift
//  TableViewConcept
//
//  Created by Rahul Kumawat on 12/11/21.
//

import UIKit

class CustomPageViewController: UIPageViewController {

    var individualPageViewControllerList = [UIViewController]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        self.delegate = self
        // Do any additional setup after loading the view.
        individualPageViewControllerList = [
            PageDetailViewController.getInstance(index: 0),
            PageDetailViewController.getInstance(index: 1),
            PageDetailViewController.getInstance(index: 2),
            PageDetailViewController.getInstance(index: 3),
            PageDetailViewController.getInstance(index: 4),
            PageDetailViewController.getInstance(index: 5)
        ]
        setViewControllers([individualPageViewControllerList[0]], direction: .forward, animated: true, completion: nil)
        let pageControl = UIPageControl.appearance()
        pageControl.pageIndicatorTintColor = .lightGray
        pageControl.currentPageIndicatorTintColor = .black
    }
    
}

extension CustomPageViewController: UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let indexOfCurrentPageViewContainer = individualPageViewControllerList.firstIndex(of: viewController)!
        if indexOfCurrentPageViewContainer == 0 {
            return nil
        }
        else{
            return individualPageViewControllerList[indexOfCurrentPageViewContainer - 1]
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let currentIndexOfPageViewController = individualPageViewControllerList.firstIndex(of: viewController)!
        if currentIndexOfPageViewController == individualPageViewControllerList.count - 1 {
            return nil
        }
        else{
            return individualPageViewControllerList[currentIndexOfPageViewController + 1]
        }
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return 6
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
}
