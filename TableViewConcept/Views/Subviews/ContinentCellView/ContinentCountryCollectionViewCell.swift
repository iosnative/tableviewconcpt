//
//  ContinentCountryCollectionViewCell.swift
//  TableViewConcept
//
//  Created by Rahul Kumawat on 11/11/21.
//

import UIKit

class ContinentCountryCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var CountryLabel: UILabel!
    @IBOutlet weak var ContinentLabel: UILabel!
    
    var modelContinentCountryName: ContinentCountryModel?{
        didSet{
            countryConfiguration()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func countryConfiguration(){
        CountryLabel.text = modelContinentCountryName?.country
        ContinentLabel.text = modelContinentCountryName?.continent
    }
    
}
