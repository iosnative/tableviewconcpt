//
//  ContinentTableViewCell.swift
//  TableViewConcept
//
//  Created by Rahul Kumawat on 10/11/21.
//

import UIKit

class ContinentTableViewCell: UITableViewCell {

    // View which contains the loading text and the spinner
    let loadingView = UIView()
    // Spinner shown during load the TableView
    let spinner = UIActivityIndicatorView()
    
    @IBOutlet weak var tableContainerView: UIView!
    @IBOutlet weak var tableContinentName: UILabel!
    @IBOutlet weak var tableContinentDetailButton: UIButton!
    @IBOutlet weak var tableContinentCollectionView: UICollectionView!
    @IBOutlet weak var tableContinentCount: UILabel!
    
    var viewModelCountry = CountryViewModel()
    var tableCellIndex:Int = 0
    var localCountryData  = [ContinentCountryModel]()
    
    weak var navigationController: UINavigationController?

    
    var modelContinentName: ContinentNameModel?{
        didSet{
            countryConfiguration()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        tableContinentCollectionView.register(UINib(nibName: "ContinentCountryCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ContinentCountryCollectionViewCell")
        tableContinentCollectionView.delegate = self
        tableContinentCollectionView.dataSource = self
        viewModelCountry.continenttablevc = self
        setLoadingScreen()
        loadData()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func loadData(){
        DispatchQueue.main.async {
            self.viewModelCountry.getContinentCountryName()
            self.viewModelCountry.getStatesData()
        }
    }
    
    // Set the activity indicator into the main view
    func setLoadingScreen() {
        // Sets the view which contains the loading text and the spinner
        let width: CGFloat = 30
        let height: CGFloat = 30
        let x = (UIScreen.main.bounds.width / 2) - (width / 2)
        let y = (tableContinentCollectionView.frame.height / 2) - (height / 2)
        loadingView.frame = CGRect(x: x, y: y, width: width, height: height)
        
        // Sets spinner
        spinner.style = .large
        spinner.color = .systemRed
        spinner.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        spinner.startAnimating()
        
        // Adds text and spinner to the view
        loadingView.addSubview(spinner)
        tableContinentCollectionView.addSubview(loadingView)
    }
    
    // Remove the activity indicator from the main view
    func removeLoadingScreen() {
        // Hides and stops the text and the spinner
        spinner.stopAnimating()
        spinner.isHidden = true
    }
    
    func countryConfiguration(){
        tableContinentName.text = "\(modelContinentName?.country ?? "") - \(modelContinentName?.code ?? "")"
        tableContinentCount.text = "\(modelContinentName?.count ?? 0)"
    }
    
}

extension ContinentTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            return CGSize(width: 200, height: 300)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        for country in viewModelCountry.continentCountryArray {
            if(modelContinentName?.country == country.continent){
                localCountryData.append(country)
            }
        }
        return localCountryData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ContinentCountryCollectionViewCell", for: indexPath) as? ContinentCountryCollectionViewCell
        cell?.modelContinentCountryName = localCountryData[indexPath.row]
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        print("Index: ",indexPath)
//        print("Country1: ",modelContinentName as Any)
//        print("Country2: ",localCountryData[indexPath.row])
        didTapSelectedState(index: indexPath.row)
    }
    
    func didTapSelectedState(index: Int) {
        let vc = StatesViewController.getInstance
        vc.modalPresentationStyle = .fullScreen
        vc.continentName = "\(modelContinentName?.country ?? "Data not found") - \(modelContinentName?.code ?? "")"
        let xTotalCountry : Int = modelContinentName?.count ?? 0
        let xTotalCountryNSNumber = xTotalCountry as NSNumber
        var xTotalCountryString : String = ""
        if(xTotalCountry == 0){
            xTotalCountryString = "Data not found"
        }
        else{
            xTotalCountryString = xTotalCountryNSNumber.stringValue
        }
        vc.totalCountry = xTotalCountryString
        var countryName = ""
        var statesList : [String] = []
        for country in viewModelCountry.statesArray {
            if(localCountryData[index].country == country.country){
                countryName = country.country!
                statesList = country.states!
            }
        }
        vc.countryName = countryName
        let xTotalStates : Int = statesList.count
        let xTotalStatesNSNumber = xTotalStates as NSNumber
        var xTotalStatesString : String = ""
        if(xTotalCountry == 0){
            xTotalStatesString = "Data not found"
        }
        else{
            xTotalStatesString = xTotalStatesNSNumber.stringValue
        }
        vc.totalStates = xTotalStatesString
        vc.states = statesList
        navigationController?.pushViewController(vc, animated: true)
    }
    
}
