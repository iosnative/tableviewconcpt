//
//  CountryCollectionViewCell.swift
//  TableViewConcept
//
//  Created by Rahul Kumawat on 09/11/21.
//

import UIKit
import SVGKit

class CountryCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imageViewLabel: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    var modelCountry: CountryModel?{
        didSet{
            countryConfiguration()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func countryConfiguration(){
        nameLabel.text = modelCountry?.code
        let svgURL = URL(string: (modelCountry?.image)!)
        let data = try? Data(contentsOf: svgURL!)
        let anSVGImage: SVGKImage = SVGKImage(data: data!)
        imageViewLabel.image = anSVGImage.uiImage
    }

}
