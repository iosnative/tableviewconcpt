//
//  CountryCell.swift
//  TableViewConcept
//
//  Created by Rahul Kumawat on 02/11/21.
//

import UIKit
import SVGKit

class CountryCell: UITableViewCell {

    @IBOutlet weak var countainerView: UIView!
    @IBOutlet weak var headingTitleLabel: UILabel!
    @IBOutlet weak var subHeadingTitleLabel: UILabel!
    @IBOutlet weak var imageViewLabel: UIImageView!
    
    
    var modelCountry: CountryModel?{
        didSet{
            countryConfiguration()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
        
    }
    
    func countryConfiguration(){
        headingTitleLabel.text = modelCountry?.name
        subHeadingTitleLabel.text = modelCountry?.code
        let svgURL = URL(string: (modelCountry?.image)!)
        let data = try? Data(contentsOf: svgURL!)
        let anSVGImage: SVGKImage = SVGKImage(data: data!)
        imageViewLabel.image = anSVGImage.uiImage
    }

}
