//
//  SearchViewController.swift
//  TableViewConcept
//
//  Created by Rahul Kumawat on 03/11/21.
//

import UIKit

class SearchViewController: UIViewController, UISearchBarDelegate, UITextFieldDelegate {
    var dataDelegate: TableDataProtocol?
    
    var countryLocalArray = [CountryModel]()
    var filteredCountryArray : [CountryModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addSearchBar()
    }
    
    func addSearchBar() {
        let searchBar:UISearchBar = UISearchBar()
        //IF you want frame replace first line and comment "searchBar.sizeToFit()"
        //let searchBar:UISearchBar = UISearchBar(frame: CGRect(x: 10, y: 10, width: headerView.frame.width-20, height: headerView.frame.height-20))
        searchBar.searchBarStyle = UISearchBar.Style.prominent
        searchBar.placeholder = " Search Country..."
        searchBar.sizeToFit()
        searchBar.isTranslucent = true
        searchBar.backgroundImage = UIImage()
        searchBar.delegate = self
        self.view.addSubview(searchBar)//Here change your view name
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count == 0 {
            filteredCountryArray = []
            self.dataDelegate?.didFilterData(isFiltered: false)
        } else {
            
            //filteredCountryArray = countryLocalArray.filter { $0.name?.lowercased() == searchText.lowercased() }
            filteredCountryArray = countryLocalArray.filter { $0.name?.lowercased().contains(searchText.lowercased()) as! Bool }
            self.dataDelegate?.didReceiveData(data: filteredCountryArray)
            //print("F ARRAY => ",filteredCountryArray)
            self.dataDelegate?.didFilterData(isFiltered: true)
        }
        //print("ARRAY -> ",filteredCountryArray.count)
    }
}
