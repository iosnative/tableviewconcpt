//
//  StateTableViewCell.swift
//  TableViewConcept
//
//  Created by Rahul Kumawat on 15/11/21.
//

import UIKit

class StateTableViewCell: UITableViewCell {
    
    @IBOutlet weak var stateNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    

}
