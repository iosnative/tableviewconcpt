//
//  ContinentNameViewModel.swift
//  TableViewConcept
//
//  Created by Rahul Kumawat on 10/11/21.
//

import Foundation

struct ContinentNameModel : Codable {
    let code : String?
    let count : Int?
    let country : String?

    enum CodingKeys: String, CodingKey {

        case code = "code"
        case count = "count"
        case country = "country"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(String.self, forKey: .code)
        count = try values.decodeIfPresent(Int.self, forKey: .count)
        country = try values.decodeIfPresent(String.self, forKey: .country)
    }
}
