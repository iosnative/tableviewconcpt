//
//  ContinentCountryModel.swift
//  TableViewConcept
//
//  Created by Rahul Kumawat on 10/11/21.
//

import Foundation

struct ContinentCountryModel : Codable {
    let continent : String?
    let country : String?

    enum CodingKeys: String, CodingKey {

        case continent = "continent"
        case country = "country"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        continent = try values.decodeIfPresent(String.self, forKey: .continent)
        country = try values.decodeIfPresent(String.self, forKey: .country)
    }

}
