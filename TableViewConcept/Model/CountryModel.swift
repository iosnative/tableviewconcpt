//
//  CountryModel.swift
//  TableViewConcept
//
//  Created by Rahul Kumawat on 02/11/21.
//

import Foundation

struct CountryModel : Codable {
    let code : String?
    let name : String?
    let emoji : String?
    let image : String?
    let unicode : String?

    enum CodingKeys: String, CodingKey {

        case code = "code"
        case name = "name"
        case emoji = "emoji"
        case image = "image"
        case unicode = "unicode"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(String.self, forKey: .code)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        emoji = try values.decodeIfPresent(String.self, forKey: .emoji)
        image = try values.decodeIfPresent(String.self, forKey: .image)
        unicode = try values.decodeIfPresent(String.self, forKey: .unicode)
    }

}
