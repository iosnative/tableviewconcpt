//
//  StatesModel.swift
//  TableViewConcept
//
//  Created by Rahul Kumawat on 15/11/21.
//

import Foundation

struct StatesModel : Codable {
    let states : [String]?
    let country : String?

    enum CodingKeys: String, CodingKey {

        case states = "states"
        case country = "country"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        states = try values.decodeIfPresent([String].self, forKey: .states)
        country = try values.decodeIfPresent(String.self, forKey: .country)
    }

}
