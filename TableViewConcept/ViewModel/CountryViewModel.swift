//
//  USerViewMOdel.swift
//  TableViewConcept
//
//  Created by Rahul Kumawat on 02/11/21.
//

import Foundation

class CountryViewModel{
    
    weak var tablevc: MainTableViewController?
    weak var collectionvc: CollectionViewController?
    weak var continentvc: ContinentViewController?
    weak var continenttablevc: ContinentTableViewCell?
    weak var statetablevc: StatesViewController?
    
    var countryArray = [CountryModel]()
    var continentNameArray = [ContinentNameModel]()
    var continentCountryArray = [ContinentCountryModel]()
    var statesArray = [StatesModel]()

    let countryURL = "https://api.npoint.io/abf69e859c51ba4a19e4"
    let continentNameURL = "https://api.npoint.io/0afd86e2dc35ae621953"
    let continentCountryURL = "https://api.npoint.io/3f9e224245960346bd0c"
    let statesByCountryURL = "https://api.npoint.io/37eb8d9e4b0da0f074ce"
    
    func getAllCountryData(){
        URLSession.shared.dataTask(with: URL(string: countryURL)!) { (data, response, error) in
            if error == nil{
                if let data = data {
                    do{
                        let countryResponse = try JSONDecoder().decode([CountryModel].self, from: data)
                        self.countryArray.append(contentsOf: countryResponse)
                        DispatchQueue.main.async{
                            self.tablevc?.tableView.reloadData()
                            self.collectionvc?.collectionView.reloadData()
                            self.tablevc?.removeLoadingScreen()
                            self.collectionvc?.removeLoadingScreen()
                        }
                    }catch let err{
                        print(err.localizedDescription)
                    }
                }
            }else{
                print(error?.localizedDescription ?? "ERROR")
            }
        }.resume()
    }
    
    func getContinentName() {
        URLSession.shared.dataTask(with: URL(string: continentNameURL)!) { (data, response, error) in
            if error == nil{
                if let data = data {
                    do{
                        let continentNameResponse = try JSONDecoder().decode([ContinentNameModel].self, from: data)
                        self.continentNameArray.append(contentsOf: continentNameResponse)
                        DispatchQueue.main.async{
                            self.continentvc?.cTableView.reloadData()                        }
                    }catch let err{
                        print(err.localizedDescription)
                    }
                }
            }else{
                print(error?.localizedDescription ?? "ERROR")
            }
        }.resume()
    }
    
    func getContinentCountryName() {
        URLSession.shared.dataTask(with: URL(string: continentCountryURL)!) { (data, response, error) in
            if error == nil{
                if let data = data {
                    do{
                        let continentNameResponse = try JSONDecoder().decode([ContinentCountryModel].self, from: data)
                        self.continentCountryArray.append(contentsOf: continentNameResponse)
                        DispatchQueue.main.async{
                            self.continenttablevc?.tableContinentCollectionView.reloadData()
                            self.continenttablevc?.removeLoadingScreen()
                        }
                    }catch let err{
                        print(err.localizedDescription)
                    }
                }
            }else{
                print(error?.localizedDescription ?? "ERROR")
            }
        }.resume()
    }
    
    func getStatesData() {
        URLSession.shared.dataTask(with: URL(string: statesByCountryURL)!) { (data, response, error) in
            if error == nil{
                if let data = data {
                    do{
                        let statesResponse = try JSONDecoder().decode([StatesModel].self, from: data)
                        self.statesArray.append(contentsOf: statesResponse)
                        DispatchQueue.main.async{
                            
                        }
                    }catch let err{
                        print(err.localizedDescription)
                    }
                }
            }else{
                print(error?.localizedDescription ?? "ERROR")
            }
        }.resume()
    }
    
}
